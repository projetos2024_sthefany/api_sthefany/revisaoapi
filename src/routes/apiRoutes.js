const router = require("express").Router();

const alunoController = require("../controller/alunoController");
const teacherController = require("../controller/teacherController");
const JSONPlaceholderController = require("../controller/JSONPlaceholderController");
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");

router.get("/docente/", teacherController.getTeachers);
router.post("/cadastroaluno/", alunoController.postAluno);
router.put("/updatealuno/", alunoController.updateAluno);
router.delete("/deletealuno/:id", alunoController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebsiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebsiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebsiteNET);

router.get("/external/filter", JSONPlaceholderController.getCountDomain);

// api com banco de dados, rota para consulta das tabelas do banco
router.get("/tables", dbController.getNameTables);
router.get("/tablesdescriptions", dbController.getTablesDescription); //rota para consulta das descrições da tabela do banco

//rota para cadastro de cliente
router.post("/cadastroCliente/", clienteController.createCliente);

//rota para select da tabela cliente
router.get("/selectCliente/", clienteController.getAllClientes);

//rota para select da tabela cliente com filtro
router.get("/filtroCliente/", clienteController.getAllClientes2);

module.exports = router;
