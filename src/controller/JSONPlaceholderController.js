const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      res.status(200).json({
        message:
          "Aqui estão os usuários captados da Api pública JSONPlaceholder",
        users,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }

  static async getUsersWebsiteIO(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".io")
      );
      const Banana = users.length;
      res.status(200).json({
        message:
          "AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",
        Banana,
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getUsersWebsiteCOM(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".com")
      );
      res.status(200).json({
        message: "AQUI ESTÃO OS USERS COM DOMINIO COM: ",
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getUsersWebsiteNET(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user) =>
        user.website.endsWith(".net")
      );
      res.status(200).json({
        message: "AQUI ESTÃO OS USERS COM DOMINIO NET:",
        users,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
  }

  static async getCountDomain (req, res) {
    const { dominio } = req.query;
    if (!dominio){
      res.status(400).json({ error: "Parâmetro 'dominio' não fornecido", error });
    }
      const response = await axios.get("https://jsonplaceholder.typicode.com/users" );
      const users = response.data.filter(user => user.website.endsWith(dominio));

      const count = users.length;

      
      res.status(200).json({
    message:`AQUI ESTÃO OS USERS COM DOMINIO .${dominio}: ${count}`, users
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: "Deu ruim", error });
    }
};