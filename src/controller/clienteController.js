//Obtendo a conexão com o banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;
    // verificando se o atriburto chave é diferente de 0 (zero)
    if (telefone !== 0) {
      const query = `insert into cliente(
                telefone,
                nome,
                cpf,
                logradouro,
                numero,
                complemento,
                bairro,
                cidade,
                estado,
                cep,
                referencia) 
                values 
                ('${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}')`; //Fim da query
      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res
              .status(500)
              .json({ error: "Usuário não cadastrado no banco!!!" });
            return;
          } //Fim do if
          console.log("Inserido no banco!!!");
          res
            .status(201)
            .json({ message: "Usuário cadastrado com sucesso!!!" });
        }); //Fim do connect.query
      } catch (error) {
        console.error("Erro ao executar o insert!!!", error);
        res.status(500).json({ error: "Erro interno do servidor!!!" });
      } //Fim do catch
    } //Fim do if
    else {
      res.status(400).json({ message: "Telefone obrigatório!!!" });
    } //Fim do else
  } //Fim do createCliente

  //select da tabela cliente
  static async getAllClientes(req, res) {
    const query = `select * from pizza`;
    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usúarios não encontrados no banco!!" });
          return; //Retorna e para
        } //Fim do if
        let clientes = data;
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ clientes });
      }); //Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
      return;
    } //Fim do catch
  } //Fim getAllClientes

  //Método pra selecionar ambientes via parâmetros específicos
  static async getAllClientes2(req, res) {
    //Extrair parâmetros da consulta da URL
    const { filtro, ordenacao } = req.query;
    //Construir a consulta SQL base
    let query = `select * from cliente`;
    //Adicionar a cláusula wher,quando houver
    if (filtro) {
      //query = query + filtro; - está incorreto
      //query = query + ` where${filtro}`;
      query += ` where ${filtro}`;
    }

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usúarios não encontrados no banco!!" });
          return; //Retorna e para
        } //Fim do if
      
        console.log("Consulta realizada com sucesso!!!");
        res.status(201).json({ result });
      }); //Fim do connect query
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!!!" });
      return;
    } //Fim do catch
  } //Fim da getAllClientes2
}; // Fim do module
